package es.uva.tfg.demo.tvshows.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.VerticalLayout;

import es.uva.tfg.demo.tvshows.component.GridComponent;
import es.uva.tfg.demo.tvshows.component.TvShowComponent;
import es.uva.tfg.demo.tvshows.exception.JsonNodeViewNotFoundException;
import es.uva.tfg.demo.tvshows.json.JsonTemplates;
import es.uva.tfg.demo.tvshows.model.ContainerTvShows;
import es.uva.tfg.demo.tvshows.model.TvShow;
import es.uva.tfg.demo.tvshows.ui.TvShowsUI;
import es.uva.tfg.generadorvistas.components.JSONAbstractOrderedLayout;
import es.uva.tfg.generadorvistas.translater.JSONTranslater;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class MainView extends CustomComponent implements View {

	private static final long serialVersionUID = 5967973854186272090L;

	private static final Logger LOG = LoggerFactory.getLogger(MainView.class);

	@Autowired
	private JsonTemplates jsonTemplates;

	@Autowired
	private ContainerTvShows containerTvShows;

	@Autowired
	private ApplicationContext context;

	@PostConstruct
	public void onInit() {
		VerticalLayout content = new VerticalLayout();
		content.setWidth(100, Unit.PERCENTAGE);
		content.setHeightUndefined();
		content.addStyleName("home-view");

		VerticalLayout home = getHomeView();
		content.addComponent(home);

		setCompositionRoot(content);
		setWidth(100, Unit.PERCENTAGE);
		setHeightUndefined();
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}

	private VerticalLayout getHomeView() {
		VerticalLayout homeView = new VerticalLayout();

		try {
			JSONTranslater translater = new JSONTranslater();
			translater.setJson(jsonTemplates.getHomeView());

			JSONAbstractOrderedLayout orderedLayout = (JSONAbstractOrderedLayout) translater
					.translate();

			modifyView(orderedLayout);

			homeView = (VerticalLayout) orderedLayout.getVaadinComponent();
		} catch (JSONValidationException e) {
			LOG.error("Error al traducir el json del home:" + e.getMessage());
		} catch (JsonNodeViewNotFoundException e) {
			LOG.error("Error al obtener el json de la vista home: "
					+ e.getMessage());
		}

		return homeView;
	}

	private List<TvShowComponent> getTvShowComponents() {
		List<TvShow> tvShows = containerTvShows.getTvShows();
		List<TvShowComponent> components = new ArrayList<>();

		for (TvShow tvShow : tvShows) {
			TvShowComponent component = context.getBean(TvShowComponent.class);
			component.setTvShow(tvShow);

			components.add(component);
		}

		return components;
	}

	private void modifyView(JSONAbstractOrderedLayout orderedLayout) {
		setGridLayout(orderedLayout);
		setLogo(orderedLayout);
		setLogoutListener(orderedLayout);
		setProfileImage(orderedLayout);
	}

	private void setGridLayout(JSONAbstractOrderedLayout orderedLayout) {
		VerticalLayout gridContent = (VerticalLayout) orderedLayout
				.getVaadinComponent("gridContent");

		GridComponent gridLayout = new GridComponent(2);
		gridLayout.setSizeFull();

		List<TvShowComponent> components = getTvShowComponents();
		for (TvShowComponent component : components) {
			gridLayout.addComponent(component);
		}

		gridContent.addComponent(gridLayout);
	}

	private void setLogo(JSONAbstractOrderedLayout orderedLayout) {
		HorizontalLayout hlLogo = (HorizontalLayout) orderedLayout
				.getVaadinComponent("hlLogo");

		Image logo = new Image(null, new ThemeResource("img/tvshows.jpg"));
		logo.setSizeFull();

		hlLogo.addComponent(logo);
		hlLogo.setExpandRatio(logo, 1.0f);
	}

	private void setLogoutListener(JSONAbstractOrderedLayout orderedLayout) {
		Button btnLogout = (Button) orderedLayout
				.getVaadinComponent("btnLogout");

		btnLogout.addClickListener(event -> {
			getUI().getNavigator().navigateTo(TvShowsUI.LOGIN_VIEW_NAME);
		});
	}
	
	private void setProfileImage(JSONAbstractOrderedLayout orderedLayout) {
		HorizontalLayout hlProfileImage = (HorizontalLayout) orderedLayout
				.getVaadinComponent("hlProfileImage");

		Image profile = new Image(null, new ThemeResource("img/profile.png"));
		profile.setSizeFull();

		hlProfileImage.addComponent(profile);
		hlProfileImage.setExpandRatio(profile, 1.0f);
	}

}
