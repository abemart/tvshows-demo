package es.uva.tfg.demo.tvshows.json;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;

import es.uva.tfg.demo.tvshows.exception.JsonNodeViewNotFoundException;

@Service
public class JsonTemplates {

	@Autowired
	private PathTemplates pathTemplates;

	public JsonNode getLoginView() throws JsonNodeViewNotFoundException {
		JsonNode loginView = null;

		try {
			String path = pathTemplates.getPathTemplates();
			loginView = JsonLoader.fromResource(path + "login.json");
		} catch (IOException e) {
			throw new JsonNodeViewNotFoundException(e.getMessage());
		}

		return loginView;
	}

	public JsonNode getHomeView() throws JsonNodeViewNotFoundException {
		JsonNode homeView = null;

		try {
			String path = pathTemplates.getPathTemplates();
			homeView = JsonLoader.fromResource(path + "home.json");
		} catch (IOException e) {
			throw new JsonNodeViewNotFoundException(e.getMessage());
		}

		return homeView;
	}

	public JsonNode getTvShowComponent() throws JsonNodeViewNotFoundException {
		JsonNode homeView = null;

		try {
			String path = pathTemplates.getPathTemplates();
			homeView = JsonLoader.fromResource(path + "tvShowComponent.json");
		} catch (IOException e) {
			throw new JsonNodeViewNotFoundException(e.getMessage());
		}

		return homeView;
	}
}
