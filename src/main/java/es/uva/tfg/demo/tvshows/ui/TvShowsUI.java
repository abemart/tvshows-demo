package es.uva.tfg.demo.tvshows.ui;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;

import es.uva.tfg.demo.tvshows.view.LoginView;
import es.uva.tfg.demo.tvshows.view.MainView;

@SpringUI
@Theme("tvshowsTheme")
public class TvShowsUI extends UI {

	private static final long serialVersionUID = -8473524832675308221L;

	public static final String LOGIN_VIEW_NAME = "";
	public static final String MAIN_VIEW_NAME = "home";

	@Autowired
	private LoginView loginView;

	@Autowired
	private MainView mainView;

	private Navigator navigator;

	@Override
	protected void init(VaadinRequest request) {

		navigator = new Navigator(this, this);

		navigator.addView(LOGIN_VIEW_NAME, loginView);
		navigator.addView(MAIN_VIEW_NAME, mainView);
	}

}
