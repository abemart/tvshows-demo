package es.uva.tfg.demo.tvshows.model;

import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;

public class TvShow {

	private String title;
	private String description;
	private Resource image;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Resource getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = new ThemeResource("img/series/" + image);
	}

	@Override
	public String toString() {
		return "TvShow [title=" + title + ", description=" + description
				+ ", image=" + image + "]";
	}

}
