package es.uva.tfg.demo.tvshows.exception;

public class JsonNodeViewNotFoundException extends Exception {

	private static final long serialVersionUID = 3779689962903768160L;

	private String mensaje;

	public JsonNodeViewNotFoundException(String mensaje) {
		this.mensaje = mensaje;
	}

	@Override
	public String getMessage() {
		return mensaje;
	}

}
