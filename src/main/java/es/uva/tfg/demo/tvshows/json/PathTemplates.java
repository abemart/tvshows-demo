package es.uva.tfg.demo.tvshows.json;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = BeanDefinition.SCOPE_SINGLETON)
public class PathTemplates {

	public enum TypePath {
		TEMPLATE_PATH1("/templates/option1/"), TEMPLATE_PATH2(
				"/templates/option2/");

		private String path;

		TypePath(String path) {
			this.path = path;
		}

		public String getPath() {
			return path;
		}
	}

	private TypePath pathTemplates;

	@PostConstruct
	public void onInit() {
		pathTemplates = TypePath.TEMPLATE_PATH1;
	}

	public void setPathTemplates(TypePath pathTemplates) {
		this.pathTemplates = pathTemplates;
	}

	public String getPathTemplates() {
		return pathTemplates.getPath();
	}

}
