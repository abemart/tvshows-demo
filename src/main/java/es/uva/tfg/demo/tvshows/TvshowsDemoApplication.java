package es.uva.tfg.demo.tvshows;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TvshowsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TvshowsDemoApplication.class, args);
	}
}
