package es.uva.tfg.demo.tvshows.view;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.VerticalLayout;

import es.uva.tfg.demo.tvshows.exception.JsonNodeViewNotFoundException;
import es.uva.tfg.demo.tvshows.json.JsonTemplates;
import es.uva.tfg.demo.tvshows.ui.TvShowsUI;
import es.uva.tfg.generadorvistas.components.JSONAbstractOrderedLayout;
import es.uva.tfg.generadorvistas.translater.JSONTranslater;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;

@SpringComponent
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class LoginView extends CustomComponent implements View {

	private static final long serialVersionUID = 6316804920795324926L;

	private static final Logger LOG = LoggerFactory.getLogger(LoginView.class);

	@Autowired
	private JsonTemplates jsonTemplates;

	@PostConstruct
	public void init() {
		VerticalLayout content = new VerticalLayout();
		content.setSizeFull();

		content.addComponent(getLoginView());

		setCompositionRoot(content);
		setSizeFull();
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}

	/**
	 * Obtiene la vista del login según el tipo de template que está
	 * seleccionada.
	 * 
	 * @return la vista del login según el tipo de template que está
	 *         seleccionada.
	 */
	private VerticalLayout getLoginView() {
		VerticalLayout loginView = new VerticalLayout();

		try {
			JSONTranslater translater = new JSONTranslater();
			translater.setJson(jsonTemplates.getLoginView());

			JSONAbstractOrderedLayout abstractOrderedLayout = (JSONAbstractOrderedLayout) translater
					.translate();

			modifyView(abstractOrderedLayout);

			loginView = (VerticalLayout) abstractOrderedLayout
					.getVaadinComponent();
		} catch (JSONValidationException e) {
			LOG.error("Error al traducir el json del login:" + e.getMessage());
		} catch (JsonNodeViewNotFoundException e) {
			LOG.error("Error al obtener el json de la vista login: "
					+ e.getMessage());
		}

		return loginView;
	}

	private void modifyView(JSONAbstractOrderedLayout abstractOrderedLayout) {
		setLogo(abstractOrderedLayout);
		setPasswordField(abstractOrderedLayout);
		setTitle(abstractOrderedLayout);

		// Añadimos el click listener al botón de aceptar
		addClickListener(abstractOrderedLayout);
	}

	private void setLogo(JSONAbstractOrderedLayout abstractOrderedLayout) {
		HorizontalLayout hlIcon = (HorizontalLayout) abstractOrderedLayout
				.getVaadinComponent("hlLogo");

		Image logo = new Image(null, new ThemeResource("img/tvshows.jpg"));
		logo.setSizeFull();

		hlIcon.addComponent(logo);
		hlIcon.setExpandRatio(logo, 1.0f);
	}

	private void setPasswordField(
			JSONAbstractOrderedLayout abstractOrderedLayout) {
		VerticalLayout contenedorPassword = (VerticalLayout) abstractOrderedLayout
				.getVaadinComponent("tfPassword");

		PasswordField passwordField = new PasswordField("Contraseña");
		passwordField.setWidth(100, Unit.PERCENTAGE);

		contenedorPassword.addComponent(passwordField);
	}

	private void setTitle(JSONAbstractOrderedLayout abstractOrderedLayout) {
		HorizontalLayout contenedorTitle = (HorizontalLayout) abstractOrderedLayout
				.getVaadinComponent("hlLabel");

		Label lbTitle = new Label("TV Shows");

		contenedorTitle.addComponent(lbTitle);
	}

	private void addClickListener(
			JSONAbstractOrderedLayout abstractOrderedLayout) {
		Button btnAceptar = (Button) abstractOrderedLayout
				.getVaadinComponent("btnAceptar");

		btnAceptar.addClickListener(event -> {
			Navigator navigator = getUI().getNavigator();
			navigator.navigateTo(TvShowsUI.MAIN_VIEW_NAME);
		});
	}

}
