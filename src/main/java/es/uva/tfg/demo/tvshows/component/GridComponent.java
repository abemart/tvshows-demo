package es.uva.tfg.demo.tvshows.component;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

public class GridComponent extends CustomComponent {

	private static final long serialVersionUID = 5016659796636306478L;

	private int columns;

	private List<HorizontalLayout> rows;
	private VerticalLayout content;

	public GridComponent(int columns) {
		this.columns = columns;
		this.rows = new ArrayList<>();

		init();
	}

	private void init() {
		content = new VerticalLayout();
		content.setSizeFull();

		setCompositionRoot(content);
		setSizeFull();
	}

	public void addComponent(Component component) {
		if (rows.isEmpty()) {
			HorizontalLayout row = createRow();
			row.addComponent(component);

			rows.add(row);
			content.addComponent(row);
		} else {
			// Obtenemos la ultima fila
			HorizontalLayout row = rows.get(rows.size() - 1);

			if (row.getComponentCount() < columns) {
				row.addComponent(component);
			} else {
				row = createRow();
				row.addComponent(component);

				rows.add(row);
				content.addComponent(row);
			}
		}
	}

	private HorizontalLayout createRow() {
		HorizontalLayout row = new HorizontalLayout();
		row.setWidth(100, Unit.PERCENTAGE);
		row.addStyleName("grid-row");
		row.setHeightUndefined();

		return row;
	}

}
