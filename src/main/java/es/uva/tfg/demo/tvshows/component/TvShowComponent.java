package es.uva.tfg.demo.tvshows.component;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import es.uva.tfg.demo.tvshows.exception.JsonNodeViewNotFoundException;
import es.uva.tfg.demo.tvshows.json.JsonTemplates;
import es.uva.tfg.demo.tvshows.model.TvShow;
import es.uva.tfg.demo.tvshows.view.MainView;
import es.uva.tfg.generadorvistas.components.JSONAbstractOrderedLayout;
import es.uva.tfg.generadorvistas.translater.JSONTranslater;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class TvShowComponent extends CustomComponent {

	private static final long serialVersionUID = -117471331021112634L;

	private static final Logger LOG = LoggerFactory.getLogger(MainView.class);

	@Autowired
	private JsonTemplates jsonTemplates;

	private JSONAbstractOrderedLayout orderedLayout;

	private TvShow tvShow;
	private boolean clicked;

	@PostConstruct
	public void onInit() {
		VerticalLayout content = new VerticalLayout();
		content.setSizeFull();

		clicked = false;

		content.addComponent(getTvShowComponent());

		setCompositionRoot(content);
		setSizeFull();
	}

	public void setTvShow(TvShow tvShow) {
		this.tvShow = tvShow;
		modifyComponent();
	}

	private VerticalLayout getTvShowComponent() {
		VerticalLayout result = new VerticalLayout();

		try {
			JSONTranslater translater = new JSONTranslater();
			translater.setJson(jsonTemplates.getTvShowComponent());

			orderedLayout = (JSONAbstractOrderedLayout) translater.translate();

			final VerticalLayout tvShowComponent = (VerticalLayout) orderedLayout
					.getVaadinComponent();

			// Añadimos el listener para añadir el estilo de mostrar la
			// descripción
			tvShowComponent.addLayoutClickListener(event -> {
				if (clicked) {
					clicked = !clicked;
					tvShowComponent.removeStyleName("componet-tvshow-hover");
				} else {
					clicked = !clicked;
					tvShowComponent.addStyleName("componet-tvshow-hover");
				}
			});

			result = tvShowComponent;
		} catch (JSONValidationException e) {
			LOG.error("Error al traducir el json del componente tvShow:"
					+ e.getMessage());
		} catch (JsonNodeViewNotFoundException e) {
			LOG.error("Error al obtener el json del componente tvShow: "
					+ e.getMessage());
		}

		return result;
	}

	private void modifyComponent() {
		setImage();
		setTitle();
		setDescription();
	}

	private void setImage() {
		VerticalLayout vlImage = (VerticalLayout) orderedLayout
				.getVaadinComponent("vlImage");

		Image image = new Image(null, tvShow.getImage());

		vlImage.addComponent(image);
		vlImage.setExpandRatio(image, 1.0f);
	}

	private void setTitle() {
		VerticalLayout vlTitle = (VerticalLayout) orderedLayout
				.getVaadinComponent("vlTitle");

		Label lbTitle = new Label(tvShow.getTitle());

		vlTitle.addComponent(lbTitle);
	}

	private void setDescription() {
		VerticalLayout vlDescription = (VerticalLayout) orderedLayout
				.getVaadinComponent("vlDescription");

		Label lbDescription = new Label(tvShow.getDescription());

		vlDescription.addComponent(lbDescription);
	}

}
