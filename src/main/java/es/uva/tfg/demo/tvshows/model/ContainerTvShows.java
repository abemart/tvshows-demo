package es.uva.tfg.demo.tvshows.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = BeanDefinition.SCOPE_SINGLETON)
public class ContainerTvShows {

	private List<TvShow> container;

	public ContainerTvShows() {
		container = new ArrayList<>();

		TvShow breakingBad = new TvShow();
		breakingBad.setImage("breakingBad.jpeg");
		breakingBad.setTitle("Breaking Bad");
		breakingBad.setDescription(
				"Walter White es un esforzado profesor de química de secundaria, padre de Walter "
						+ "White Jr y esposo de Skyler White. Cuando la vida ya le era bastante complicada para Walter, "
						+ "le diagnostican un cáncer pulmonar terminal, lo que le lleva a quebrantar la ley e "
						+ "instalar un laboratorio de metanfetamina con su antiguo estudiante Jesse "
						+ "Pinkman para así asegurar el bienestar económico de su familia.");

		TvShow got = new TvShow();
		got.setImage("got.jpg");
		got.setTitle("Juego de Tronos");
		got.setDescription(
				"La historia transcurre en un mundo fantástico con reminiscencias de la Europa "
						+ "de la Edad Media. En el que la magia y las criaturas míticas del pasado han quedado en "
						+ "el olvido. En el continente de Poniente, donde las estaciones duran décadas y los inviernos "
						+ "son tiempos duros, se acerca el final del largo verano. ");

		TvShow houseOfCards = new TvShow();
		houseOfCards.setImage("houseCards.jpg");
		houseOfCards.setTitle("House of Cards");
		houseOfCards.setDescription(
				"Francis Underwood es un político despiadado con un puesto que le da acceso a "
						+ "información clave y, con el apoyo de su mujer, no tendrá ningún escrúpulo a la hora de "
						+ "utilizarla para alcanzar el poder y disfrutar de los privilegios asociados a él. ");

		TvShow modernFamily = new TvShow();
		modernFamily.setImage("modernFamily.jpg");
		modernFamily.setTitle("Modern Family");
		modernFamily.setDescription(
				"Una comedia con formato de falso documental que retrata a una típica "
						+ "(o atípica, según se mire) familia americana. Los protagonistas representan tres "
						+ "estructuras familiares muy diferentes: la familia convencional, la pareja homosexual "
						+ "y el hombre maduro casado con una mujer mucho más joven.");

		TvShow peakyBlinders = new TvShow();
		peakyBlinders.setImage("peakyBlinders.jpg");
		peakyBlinders.setTitle("Peaky Blinders");
		peakyBlinders.setDescription(
				"Una familia de gánsteres asentada en Birmingham tras la Primera Guerra Mundial, "
						+ "dirige un local de apuestas hípicas. Las actividades del ambicioso jefe de la banda llaman "
						+ "la atención del Inspector jefe Chester Campbell, un detective de la Real Policía Irlandesa "
						+ "que es enviado desde Belfast para limpiar la ciudad y acabar con la banda.");

		TvShow siliconValley = new TvShow();
		siliconValley.setImage("siliconValey.jpg");
		siliconValley.setTitle("Silicon Valey");
		siliconValley.setDescription(
				"En la fiebre por la alta tecnología moderna de Silicon Valley, las personas más "
						+ "cualificadas para tener éxito son las menos capaces de manejarlo. Una comedia parcialmente inspirada "
						+ "por las propias experiencias de Mike Judge como ingeniero de Silicon Valley a final de los años 1980s.");

		TvShow simpsons = new TvShow();
		simpsons.setImage("simpsons.jpg");
		simpsons.setTitle("Los Simpsons");
		simpsons.setDescription(
				"Narra la historia de una peculiar familia (Homer, Marge, Bart, Maggie y Lisa Simpson) y otros divertidos "
						+ "personajes de la localidad norteamericana de Springfield. Homer, el padre, es un desastroso inspector "
						+ "de seguridad de una central nuclear.");

		TvShow homeland = new TvShow();
		homeland.setImage("homeland.jpg");
		homeland.setTitle("Homeland");
		homeland.setDescription(
				"Cuando el Sargento de los Marines Nicholas Brody, desaparecido en combate, regresa convertido en un"
						+ " héroe ocho años después de ser capturado en Irak, la brillante pero inestable agente de la CIA Carrie Mathison "
						+ "no cree en su historia. Ella piensa que Brody trabaja para al-Qaeda y puede estar conectado con una trama terrorista "
						+ "cuyo objetivo es atacar el territorio estadounidense. ");

		container.add(got);
		container.add(breakingBad);
		container.add(houseOfCards);
		container.add(modernFamily);
		container.add(peakyBlinders);
		container.add(siliconValley);
		container.add(simpsons);
		container.add(homeland);
		container.add(houseOfCards);
		container.add(got);
		container.add(breakingBad);
		container.add(peakyBlinders);
	}

	public List<TvShow> getTvShows() {
		return container;
	}

}
